import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Desafio {

    private WebDriver driver;

    @Before
    public void main(){
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        driver.get(" https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }
    @Test
    public void fluxoPrincipal(){
        driver.findElement(By.name("username")).sendKeys("EltonFelix");
        driver.findElement(By.name("password")).sendKeys("860892");

        WebElement comments = driver.findElement(By.name("comments"));
        comments.clear();
        comments.sendKeys("simbora pro Desafio!");

        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[5]/td/input[3]")).click();
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[5]/td/input[1]")).click();

        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[6]/td/input[1]")).click();

        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[7]/td/select/option[4]")).click();
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[7]/td/select/option[2]")).click();

        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[8]/td/select/option[3]")).click();

        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[9]/td/input[2]")).click();

        //information validation
        Assert.assertEquals("EltonFelix", driver.findElement(By.id("_valueusername")).getText());
        Assert.assertEquals("860892", driver.findElement(By.id("_valuepassword")).getText());
        Assert.assertEquals("simbora pro Desafio!", driver.findElement(By.id("_valuecomments")).getText());
        Assert.assertEquals("cb1", driver.findElement(By.id("_valuecheckboxes0")).getText());
        Assert.assertEquals("rd1", driver.findElement(By.id("_valueradioval")).getText());
        Assert.assertEquals("ms2", driver.findElement(By.id("_valuemultipleselect0")).getText());
        Assert.assertEquals("dd3", driver.findElement(By.id("_valuedropdown")).getText());
    }

    @Test
    public void fluxoSecundario(){
        driver.findElement(By.name("comments")).clear();

        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[9]/td/input[2]")).click();

        //information validation
        Assert.assertEquals("No Value for username", driver.findElement(By.xpath("/html/body/div/div[3]/p[1]/strong")).getText());
        Assert.assertEquals("No Value for password", driver.findElement(By.xpath("/html/body/div/div[3]/p[2]/strong")).getText());
        Assert.assertEquals("No Value for comments", driver.findElement(By.xpath("/html/body/div/div[3]/p[3]/strong")).getText());
    }
    @After
    public void TestFim(){
        driver.quit();
    }
}
